$(function(){
    /**** Lifestyle receipents ****/
    $('.recepient').on('click',function(){
        $(this).closest('li').toggleClass('active');
        $(this).closest('li').find('recepient-options').toggleClass('active');
        $(this).toggleClass('active');
        var recepientId = $(this).attr('for');
        var recepientType = $(this).find('h5').text();
        var template = `<div class="row recepientWrapper" id="recepientWrapper-${recepientId}">
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" name="" value="${recepientType}">
                            </div>
                            <div class="col-md-4 form-group">
                                <select class="classic form-control" name="">
                                    <option value="" disabled selected>Age</option>
                                    <option value="10-20">10-20</option>
                                    <option value="20-35">20-35</option>
                                    <option value="35-50">35-50</option>
                                </select>                                            
                            </div>
                            <div class="col-md-4 form-group">
                                <select class="classic form-control" name="">
                                    <option value="" disabled selected>Personality traits</option>
                                    <option value="10-20">Personality traits 1</option>
                                    <option value="20-35">Personality traits 2</option>
                                    <option value="35-50">Personality traits 3</option>
                                </select>
                            </div>
                        </div>`;
        setTimeout(() => {
            if ($('#'+recepientId).is(":checked")) {
                $('.content-inner-bottom').append(template);
            }else{
                $('.content-inner-bottom').find('#recepientWrapper-'+recepientId).remove();
            }
        }, 10);
    });
    /**** End of Lifestyle receipents ****/

    /**** option for family member selection ****/
    // $('.familymembers-change').change(function(){
    //     var getCount = $(this).val();
    //     alert(getCount)
    // });

    /**** Lifestyle style preference ****/
    $('.rec-indv').on('click',function(){
        $(this).parent().toggleClass('active');
        var currentElement = $(this).parent();
        var skipper = $("input:checkbox[data-itemtype='styleSelector']");
        setTimeout(() => {
            if(skipper.is(":checked")){
                $(this).closest(".carousel-inner").find("input:checkbox[data-itemtype='styleSelector']").prop("checked",false);
                $(this).closest(".carousel-inner").find(".rec-indv-wrapper").removeClass('active');
                setTimeout(() => {
                    currentElement.find("input:checkbox[data-itemtype='styleSelector']").prop("checked",true);
                    currentElement.addClass('active');
                    // alert(currentElement.find("input:checkbox[data-itemtype='styleSelector']").attr('id'));
                }, 10);
            }
        }, 10);
    });
    /**** End of Lifestyle style preference ****/

    /******** Js for lifestyle-house-selection ********/
    $('.house-type').on('change',function(){
        if($(this).val() == 'new_house'){
            $(this).closest('.container').find('.new_house_details').addClass('active');
        }else{
            $(this).closest('.container').find('.new_house_details').removeClass('active');
        }
    });

    $('.get_possession_property').on('change',function(){
        if($(this).val() == 'no'){
            $(this).closest('.container').find('.tenative_possession_dates').addClass('active');
        }else{
            $(this).closest('.container').find('.tenative_possession_dates').removeClass('active');
        }
    });
    
    /******** End of Js for lifestyle-house-selection ********/

    /******** Room item selection ********/
    $('.selectProduct').change(function(){
        if($(this).is(':checked')){
            $(this).parent().addClass('checked');
        }else{
            $(this).parent().removeClass('checked');
        }
    });
});